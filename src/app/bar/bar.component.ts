import { Component, OnInit } from '@angular/core';
import {ChartOptions} from '../chart';
import { ChartService } from '../chart.service';
@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent implements OnInit {
  options={};
  chartType="bar";
  showChart:Boolean=true;
  
    constructor(private chartservice:ChartService) {
      
  }
  
  ngOnInit(){
    this.chartservice.getChartData().subscribe((val)=>{
      
    this.options=ChartOptions; 
    this.options['chart']['type']=this.chartType;
    this.options['series'][0]['data']=val;
    
     
    })

  }
}