import { Component, OnInit } from '@angular/core';
import {ChartOptions} from '../chart';
import { ChartService } from '../chart.service';
@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent implements OnInit {

  options={};
chartType="pie";
showChart:Boolean=true;

  constructor(private chartservice:ChartService) {
    
}

ngOnInit(){
  this.chartservice.getChartData().subscribe((val)=>{
    
  this.options=ChartOptions; 
  this.options['chart']['type']=this.chartType;
  this.options['series'][0]['data']=val;
  
   
  })
}


}
