import { Component, OnInit } from '@angular/core';
import {ChartOptions} from '../chart';
import { ChartService } from '../chart.service';
@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css']
})
export class LineComponent implements OnInit {

  options={};
  chartType="line";
  showChart:Boolean=true;
  
    constructor(private chartservice:ChartService) {
      
  }
  
  ngOnInit(){
    this.chartservice.getChartData().subscribe((val)=>{
      
    this.options=ChartOptions; 
    this.options['chart']['type']=this.chartType;
    this.options['series'][0]['data']=val;
    
     
    })
  }
  
  
}
