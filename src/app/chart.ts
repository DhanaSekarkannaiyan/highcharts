export const ChartOptions={
    series: [{
    //    datalabels:['sekar','djj','ddsh','hjbdhbh','hjhjhj'],
       name:'kd',
        data: []
    }],
     chart: {
plotBackgroundColor: null,
plotBorderWidth: null,
plotShadow: false,
type: ''
},
title: {
    text: 'welcome to highcharts'
  },
tooltip: {
pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
},
colors: ['#2f7ed8', '#0d233a', '#8bbc21', '#910000', 'yellow'],
plotOptions: {
pie: {
allowPointSelect: true,
cursor: 'pointer',
dataLabels: {
enabled: true,
format: '<b>{labels.name}</b>: {point.percentage:.1f} %'
}
}
}
    }
